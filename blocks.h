//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/  /*Command*/  /*Update Interval*/  /*Update Signal*/
    {"", "cat /tmp/recordingicon 2>/dev/null", 0, 14},
    {"", "statusbar-clock", 10, 1},
    {"", "statusbar-pomodoro", 1, 2},
    {"", "statusbar-player", 5, 3},
    {"", "statusbar-cpu", 10, 4},
    {"", "statusbar-mem", 10, 5},
    {"", "statusbar-gpu", 10, 6},
    {"", "statusbar-hdd", 10, 7},
    {"", "statusbar-volume", 30, 8},
    {"", "statusbar-internet", 5, 9},
    {"", "statusbar-vpn", 5, 10},
    {"", "statusbar-mail", 300, 12},
    {"", "statusbar-battery", 10, 13},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
